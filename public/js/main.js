$(document).ready(function () {
    function getQuote() {
        return $.get("https://jarks-db-tzkvu.ondigitalocean.app/jark").then(
            function (data) {  
                return data;
            }
        );
    }
    getQuote().then(
        function (text) { 
            $('#jark').html(text);
            $('#j-blockquote').fadeIn();
        }
    );
    
    $('#more').delay(5000).fadeIn();
    
    $('#more').click(function (e) { 
        e.preventDefault();
        getQuote().then(
            function (text) { 
                $('#jark').fadeOut(function () {
                $(this).html(text);
                }).fadeIn();
            }
        );
    });
});
